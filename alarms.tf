variable "monitoring_compartment_id" {
    default = "ocid1.compartment.oc1..aaaaaaaaw25brptxytn4bl3of5wumydabru6hyl534kpn3s2qvgzx4l4lqya"
}

variable "alarm_pending_duration" {
  default = "PT5M"
}

variable "alarm_resolution" {
  default = "1m"
}

variable "siebel_prod_lb_compartment_id" {
  default = "ocid1.compartment.oc1..aaaaaaaaehau25egmuo32mmrkwumltum5bnd4b2iwml5all5xlkdhlzukuwa"
} 

variable "network_compartment_id" {
  default = "ocid1.compartment.oc1..aaaaaaaaehau25egmuo32mmrkwumltum5bnd4b2iwml5all5xlkdhlzukuwa"
} 

#
# Topics
#
resource "oci_ons_notification_topic" "siebel_prod" {
  #Required
  compartment_id = var.monitoring_compartment_id
  name           = "siebel-prod"
}

resource "oci_ons_notification_topic" "siebel_non_prod" {
  #Required
  compartment_id = var.monitoring_compartment_id
  name           = "siebel-non-prod"
}

resource "oci_ons_notification_topic" "fastconnect" {
  #Required
  compartment_id = var.monitoring_compartment_id
  name           = "fastconnect"
}

#
# Load balancer alarms - production
#
resource "oci_monitoring_alarm" "siebel_prod_lb_public_bytes_sent" {
  #Required
  compartment_id        = var.monitoring_compartment_id
  destinations          = [oci_ons_notification_topic.siebel_prod.id]
  display_name          = "Siebel-Prod-LB-Public-Bytes-Sent"
  is_enabled            = true
  metric_compartment_id = var.siebel_prod_lb_compartment_id
  namespace             = "oci_lbaas"
  query                 = "BytesSent[5m]{resourceId = \"ocid1.loadbalancer.oc1.sa-saopaulo-1.aaaaaaaaiwdeczdt55ce2an7kt63yjctygnjmrrrmjwvz3ybpmyhllopqw5a\"}.mean() > 40265318"
  severity              = "CRITICAL"

  #Optional
  body                             = "Bytes enviados acima de 300 Mbps (LB de 400Mbps)"
  pending_duration                 = var.alarm_pending_duration
  resolution                       = var.alarm_resolution
}

resource "oci_monitoring_alarm" "siebel_prod_lb_public_bytes_received" {
  #Required
  compartment_id        = var.monitoring_compartment_id
  destinations          = [oci_ons_notification_topic.siebel_prod.id]
  display_name          = "Siebel-Prod-LB-Public-Bytes-Received"
  is_enabled            = true
  metric_compartment_id = var.siebel_prod_lb_compartment_id
  namespace             = "oci_lbaas"
  query                 = "BytesReceived[5m]{resourceId = \"ocid1.loadbalancer.oc1.sa-saopaulo-1.aaaaaaaaiwdeczdt55ce2an7kt63yjctygnjmrrrmjwvz3ybpmyhllopqw5a\"}.mean() > 40265318"
  severity              = "CRITICAL"

  #Optional
  body                             = "Bytes recebidos acima de 300 Mbps (LB de 400Mbps)"
  pending_duration                 = var.alarm_pending_duration
  resolution                       = var.alarm_resolution
}

resource "oci_monitoring_alarm" "siebel_prod_lb_private_bytes_sent" {
  #Required
  compartment_id        = var.monitoring_compartment_id
  destinations          = [oci_ons_notification_topic.siebel_prod.id]
  display_name          = "Siebel-Prod-LB-Private-Bytes-Sent"
  is_enabled            = true
  metric_compartment_id = var.siebel_prod_lb_compartment_id
  namespace             = "oci_lbaas"
  query                 = "BytesSent[5m]{resourceId = \"ocid1.loadbalancer.oc1.sa-saopaulo-1.aaaaaaaa4qunbqmkycuasam5hnkvl3fklkk6es72pp4raxihe5tjz6z3bb5q\", resourceId = \"ocid1.loadbalancer.oc1.sa-saopaulo-1.aaaaaaaa2evrqfg7kr4i3i3ya7qubmwfy35sqpwbdfxoqidjaptxyan37wra\"}.mean() > 40265318"
  severity              = "CRITICAL"

  #Optional
  body                             = "Bytes enviados acima de 300 Mbps (LB de 400Mbps)"
  pending_duration                 = var.alarm_pending_duration
  resolution                       = var.alarm_resolution
}

resource "oci_monitoring_alarm" "siebel_prod_lb_private_bytes_received" {
  #Required
  compartment_id        = var.monitoring_compartment_id
  destinations          = [oci_ons_notification_topic.siebel_prod.id]
  display_name          = "Siebel-Prod-LB-Private-Bytes-Received"
  is_enabled            = true
  metric_compartment_id = var.siebel_prod_lb_compartment_id
  namespace             = "oci_lbaas"
  query                 = "BytesReceived[5m]{resourceId = \"ocid1.loadbalancer.oc1.sa-saopaulo-1.aaaaaaaa4qunbqmkycuasam5hnkvl3fklkk6es72pp4raxihe5tjz6z3bb5q\", resourceId = \"ocid1.loadbalancer.oc1.sa-saopaulo-1.aaaaaaaa2evrqfg7kr4i3i3ya7qubmwfy35sqpwbdfxoqidjaptxyan37wra\"}.mean() > 40265318"
  severity              = "CRITICAL"

  #Optional
  body                             = "Bytes recebidos acima de 300 Mbps (LB de 400Mbps)"
  pending_duration                 = var.alarm_pending_duration
  resolution                       = var.alarm_resolution
}

resource "oci_monitoring_alarm" "siebel_prod_lb_unhealthy_backend_servers" {
  #Required
  compartment_id        = var.monitoring_compartment_id
  destinations          = [oci_ons_notification_topic.siebel_prod.id]
  display_name          = "Siebel-Prod-LB-UnHealthyBackendServers"
  is_enabled            = true
  metric_compartment_id = var.siebel_prod_lb_compartment_id
  namespace             = "oci_lbaas"
  query                 = "UnHealthyBackendServers[5m]{lbComponent = \"Backendset\"}.max() > 0"
  severity              = "CRITICAL"

  #Optional
  body                             = "LB - Backend Server com status UnHealthy"
  pending_duration                 = var.alarm_pending_duration
  resolution                       = var.alarm_resolution
}

resource "oci_monitoring_alarm" "siebel_prod_lb_timeouts_backend_servers" {
  #Required
  compartment_id        = var.monitoring_compartment_id
  destinations          = [oci_ons_notification_topic.siebel_prod.id]
  display_name          = "Siebel-Prod-LB-BackendTimeouts"
  is_enabled            = true
  metric_compartment_id = var.siebel_prod_lb_compartment_id
  namespace             = "oci_lbaas"
  query                 = "BackendTimeouts[5m]{lbComponent = \"Backendset\"}.max() > 0"
  severity              = "CRITICAL"

  #Optional
  body                             = "LB - Backend Servers apresentando timeouts"
  pending_duration                 = var.alarm_pending_duration
  resolution                       = var.alarm_resolution
}
#
# FASTCONNECT ALARMS
#

resource "oci_monitoring_alarm" "fastconnect_equinix_bytes_received" {
  #Required
  compartment_id        = var.monitoring_compartment_id
  destinations          = [oci_ons_notification_topic.fastconnect.id]
  display_name          = "FastConnect-Equinix-Bytes-Received"
  is_enabled            = true
  metric_compartment_id = var.network_compartment_id
  namespace             = "oci_fastconnect"
  query                 = "BytesReceived[5m]{resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaavrvcluywxl7mt7dkag6fficv4bltezd7pwogg422wwyiah352oza\", resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaalxgfcecmvzxso6sic7xehphggzbxahfyko3lcgmjqa73fna5nckq\"}.mean() > 671088640"
  severity              = "CRITICAL"

  #Optional
  body                             = "Bytes recebidos acima de 5 Gbps (FC de 10 Gbps)"
  pending_duration                 = var.alarm_pending_duration
  resolution                       = var.alarm_resolution
}

resource "oci_monitoring_alarm" "fastconnect_equinix_bytes_sent" {
  #Required
  compartment_id        = var.monitoring_compartment_id
  destinations          = [oci_ons_notification_topic.fastconnect.id]
  display_name          = "FastConnect-Equinix-Bytes-Sent"
  is_enabled            = true
  metric_compartment_id = var.network_compartment_id
  namespace             = "oci_fastconnect"
  query                 = "BytesSent[5m]{resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaavrvcluywxl7mt7dkag6fficv4bltezd7pwogg422wwyiah352oza\", resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaalxgfcecmvzxso6sic7xehphggzbxahfyko3lcgmjqa73fna5nckq\"}.mean() > 671088640"
  severity              = "CRITICAL"

  #Optional
  body                             = "Bytes enviados acima de 5 Gbps (FC de 10 Gbps)"
  pending_duration                 = var.alarm_pending_duration
  resolution                       = var.alarm_resolution
}

resource "oci_monitoring_alarm" "fastconnect_ascenty_bytes_received" {
  #Required
  compartment_id        = var.monitoring_compartment_id
  destinations          = [oci_ons_notification_topic.fastconnect.id]
  display_name          = "FastConnect-Ascenty-Bytes-Received"
  is_enabled            = true
  metric_compartment_id = var.network_compartment_id
  namespace             = "oci_fastconnect"
  query                 = "BytesReceived[5m]{resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaaguqyfwz5u2b35a34q62dlrx4o3o6pbgtztxebqp6pufqiomsthia\", resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaah35hpx6hrfgdafpqwq5jfw2b6mfoe4b4ynmmftuqkidg43xhaecq\"}.mean() > 67108864"
  severity              = "CRITICAL"

  #Optional
  body                             = "Bytes recebidos acima de 500 Mbps (FC de 1 Gbps)"
  pending_duration                 = var.alarm_pending_duration
  resolution                       = var.alarm_resolution
}

resource "oci_monitoring_alarm" "fastconnect_ascenty_bytes_sent" {
  #Required
  compartment_id        = var.monitoring_compartment_id
  destinations          = [oci_ons_notification_topic.fastconnect.id]
  display_name          = "FastConnect-Ascenty-Bytes-Sent"
  is_enabled            = true
  metric_compartment_id = var.network_compartment_id
  namespace             = "oci_fastconnect"
  query                 = "BytesSent[5m]{resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaaguqyfwz5u2b35a34q62dlrx4o3o6pbgtztxebqp6pufqiomsthia\", resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaah35hpx6hrfgdafpqwq5jfw2b6mfoe4b4ynmmftuqkidg43xhaecq\"}.mean() > 67108864"
  severity              = "CRITICAL"

  #Optional
  body                             = "Bytes enviados acima de 500 Mbps (FC de 1 Gbps)"
  pending_duration                 = var.alarm_pending_duration
  resolution                       = var.alarm_resolution
}

resource "oci_monitoring_alarm" "fastconnect_equinix_connection_state" {
  #Required
  compartment_id        = var.monitoring_compartment_id
  destinations          = [oci_ons_notification_topic.fastconnect.id]
  display_name          = "FastConnect-Equinix-State"
  is_enabled            = true
  metric_compartment_id = var.network_compartment_id
  namespace             = "oci_fastconnect"
  query                 = "ConnectionState[5m]{resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaavrvcluywxl7mt7dkag6fficv4bltezd7pwogg422wwyiah352oza\", resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaalxgfcecmvzxso6sic7xehphggzbxahfyko3lcgmjqa73fna5nckq\"}.min() < 1"
  severity              = "CRITICAL"

  #Optional
  body                             = "Link de FastConnect da Equinix Down"
  pending_duration                 = var.alarm_pending_duration
  resolution                       = var.alarm_resolution
}

resource "oci_monitoring_alarm" "fastconnect_ascenty_connection_state" {
  #Required
  compartment_id        = var.monitoring_compartment_id
  destinations          = [oci_ons_notification_topic.fastconnect.id]
  display_name          = "FastConnect-Ascenty-State"
  is_enabled            = true
  metric_compartment_id = var.network_compartment_id
  namespace             = "oci_fastconnect"
  query                 = "ConnectionState[5m]{resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaaguqyfwz5u2b35a34q62dlrx4o3o6pbgtztxebqp6pufqiomsthia\", resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaah35hpx6hrfgdafpqwq5jfw2b6mfoe4b4ynmmftuqkidg43xhaecq\"}.min() < 1"
  severity              = "CRITICAL"

  #Optional
  body                             = "Link de FastConnect da Ascenty Down"
  pending_duration                 = var.alarm_pending_duration
  resolution                       = var.alarm_resolution
}

resource "oci_monitoring_alarm" "fastconnect_equinix_packets_error" {
  #Required
  compartment_id        = var.monitoring_compartment_id
  destinations          = [oci_ons_notification_topic.fastconnect.id]
  display_name          = "FastConnect-Equinix-Packets-Error"
  is_enabled            = true
  metric_compartment_id = var.network_compartment_id
  namespace             = "oci_fastconnect"
  query                 = "PacketsError[5m]{resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaavrvcluywxl7mt7dkag6fficv4bltezd7pwogg422wwyiah352oza\", resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaalxgfcecmvzxso6sic7xehphggzbxahfyko3lcgmjqa73fna5nckq\"}.sum() > 10"
  severity              = "CRITICAL"

  #Optional
  body                             = "Link de FastConnect da Equinix com erros de pacote"
  pending_duration                 = var.alarm_pending_duration
  resolution                       = var.alarm_resolution
}

resource "oci_monitoring_alarm" "fastconnect_ascenty_packets_error" {
  #Required
  compartment_id        = var.monitoring_compartment_id
  destinations          = [oci_ons_notification_topic.fastconnect.id]
  display_name          = "FastConnect-Ascenty-Packets-Error"
  is_enabled            = true
  metric_compartment_id = var.network_compartment_id
  namespace             = "oci_fastconnect"
  query                 = "PacketsError[5m]{resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaaguqyfwz5u2b35a34q62dlrx4o3o6pbgtztxebqp6pufqiomsthia\", resourceId = \"ocid1.virtualcircuit.oc1.sa-saopaulo-1.aaaaaaaah35hpx6hrfgdafpqwq5jfw2b6mfoe4b4ynmmftuqkidg43xhaecq\"}.sum() > 10"
  severity              = "CRITICAL"

  #Optional
  body                             = "Link de FastConnect da Ascenty com erros de pacote"
  pending_duration                 = var.alarm_pending_duration
  resolution                       = var.alarm_resolution
}
